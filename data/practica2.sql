DROP DATABASE IF EXISTS practica2;
CREATE DATABASE practica2;
USE practica2;

CREATE TABLE noticias(
  id int AUTO_INCREMENT,
  titulo varchar(255),
  texto text,
  foto varchar(255),
  PRIMARY KEY(id)
  );

CREATE TABLE articulos(
  id int AUTO_INCREMENT,
  titulo varchar(255),
  texto text,
  textoLargo text,
  foto varchar(255),
  PRIMARY KEY(id)
  );

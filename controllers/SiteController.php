<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Noticias;
use app\models\Articulos;
use \yii\data\ActiveDataProvider;



class SiteController extends Controller
{    
    public function actionIndex(){
       $noticias= Noticias::find()
                  ->all();
        
        return $this->render('index',[
            "noticias"=>$noticias,
        ]);
    }
    
    public function actionArticulos(){
        $articulos= Articulos::find()
                    ->all();
        return $this->render('articulos',[
            'articulos'=>$articulos,
            
        ]);
    }
    public function actionLeer($id) {
        $articulos= Articulos::find()
                    ->where([
                        'id'=>$id,
                    ])
                    ->all();
        return $this->render('leer',[
            'articulos'=>$articulos,
        ]);
    }
    public function actionTodo(){
        $consulta= Noticias::find();
        
        $consulta1= Articulos::find();
        
        $datos=new ActiveDataProvider([
            'query'=>$consulta,
                       'pagination'=>[
                'pageSize'=>1,
                'pageParam'=>'noticia',//con esto consegui que la paginacion sea de manera independiente.
            ]
        ]);
        
        $datos1=new ActiveDataProvider([
            'query'=>$consulta1,
            'Pagination'=>[
                'PageSize'=>1
            ]
            
        ]);
        
        return $this->render('todo',[
            "dataProvider"=>$datos,
            "dataProvider1"=>$datos1,
            
        ]);
    }
}

<?php
use yii\helpers\Html;
$this->title='Articulos';

?>
<div class="row">
    <?php
        foreach($articulos as $articulo){
    ?>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <?= Html::img("@web/imgs/".$articulo->foto,['alt'=>'fotoArticulo'])?>
      <div class="caption">
        <h3><?=$articulo->titulo?></h3>
        <p><?=$articulo->texto?></p>
        <p><?=Html::a('Leer mas',['site/leer', 'id'=>$articulo->id], ['class' => 'btn btn-info']); ?></p>
      </div>
    </div>
  </div>
    <?php
        }
    ?>
</div>
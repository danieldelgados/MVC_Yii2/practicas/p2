<?php
/* @var $this yii\web\View */
//var_dump($noticias);
$this->title = 'Noticias';
use yii\helpers\Html;

?>
<div class="site-index">
    <div class="row">
        <?php
            foreach($noticias as $noticia){
        ?>
        <div class="col-sm-6 col-md-6">
            <div class="thumbnail">
                <?= Html::img("@web/imgs/".$noticia->foto,['alt'=>'fotoNoticia'])?>
                <div class="caption">
                    <h3><?=$noticia->titulo?></h3>
                    <p><?=$noticia->texto?></p>

                </div>
            </div>
        </div>
        <?php
            }
        ?>
    </div>

</div>

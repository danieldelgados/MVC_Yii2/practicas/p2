<?php

use yii\helpers\Html;

foreach ($articulos as $articulo) {
    
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= $articulo->titulo; ?></h3>
    </div>
    <div class="panel-body">
        <p><h3><?= $articulo->textoLargo; ?></h3></p>

        <div class="row">
            <div class="col-xs-6 col-md-3 ">
                
                    <?= Html::img("@web/imgs/".$articulo->foto,['alt'=>'fotoArticulo'])?>
                
            </div>
            ...
        </div>
    </div>
</div>
<?php
}
?>
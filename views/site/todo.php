<?php
use yii\grid\GridView;
use yii\helpers\Html;
?>
<h1><?= $this->title='Noticias'?></h1>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'titulo',
            'texto:ntext',
            [
            'label'=>'foto',
            'format'=>'raw',
            'value' => function($data){
                        return Html::img("@web/imgs/$data->foto",['class'=>'img-responsive']); 
                       }
            ],
            
            
        ],
    ]); ?>
    <h1><?= $this->title='Articulos'?></h1>
    <?= GridView::widget([
        'dataProvider' => $dataProvider1,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'titulo',
            'texto:ntext',            
            [
            'label'=>'foto',
            'format'=>'raw',
            'value' => function($data){
                        return Html::img("@web/imgs/$data->foto",['class'=>'img-responsive']); 
                       }
            ],

            
        ],
    ]); ?>

